<?php


$players = [
  'david' => 'David van Dijk',
  'tommie' => 'Tommie Crawford',
  'mike' => 'Mike Hens',
  'bert' => 'Bert Boerland',
  'martijn' => 'Martijn Hahn',
  'francois' => 'François van Leersum',
  'george' => 'George Moses',
  'connor' => 'Connor van Spronssen',
  'deona' => 'Deona Secreve',
];

/**
 * Games array: date, winning player, losing player.
 */
$games = [
  [ '04-09-2018', 'tommie', 'mike' ],
  [ '04-09-2018', 'george', 'francois' ],
  [ '04-09-2018', 'david', 'mike' ],
  [ '04-09-2018', 'francois', 'tommie' ],

  [ '05-09-2018', 'mike', 'george' ],
  [ '05-09-2018', 'francois', 'tommie' ],
  [ '05-09-2018', 'bert', 'tommie' ],
  [ '05-09-2018', 'francois', 'martijn' ],

  [ '06-09-2018', 'mike', 'tommie' ],
  [ '06-09-2018', 'david', 'bert' ],
  [ '06-09-2018', 'mike', 'george' ],
  [ '06-09-2018', 'francois', 'tommie' ],
  [ '06-09-2018', 'martijn', 'connor' ],
  [ '06-09-2018', 'george', 'francois' ],

  [ '07-09-2018', 'francois', 'tommie' ],
  [ '07-09-2018', 'connor', 'tommie' ],

  [ '10-09-2018', 'george', 'tommie' ],
  [ '10-09-2018', 'francois', 'connor' ],
  [ '10-09-2018', 'david', 'tommie' ],
  [ '10-09-2018', 'mike', 'martijn' ],

  [ '11-09-2018', 'connor', 'george' ],
  [ '11-09-2018', 'tommie', 'david' ],
  [ '11-09-2018', 'tommie', 'connor' ],
  [ '11-09-2018', 'mike', 'david' ],
  [ '11-09-2018', 'tommie', 'francois' ],

  [ '12-09-2018', 'tommie', 'mike' ],
  [ '12-09-2018', 'connor', 'martijn' ],

  [ '13-09-2018', 'martijn', 'mike' ],
  [ '13-09-2018', 'connor', 'deona' ],
  [ '13-09-2018', 'francois', 'martijn' ],
  [ '13-09-2018', 'tommie', 'david' ],
  [ '13-09-2018', 'david', 'francois' ],
  [ '13-09-2018', 'martijn', 'tommie' ],
  [ '13-09-2018', 'george', 'connor' ],
];

file_put_contents('players.json', json_encode($players));
file_put_contents('games.json', json_encode($games));
