<?php

define('K_FACTOR', 64);

/**
 * Calculates the bonus and penalty scores given current scores of players.
 *
 * @param float $score_winner
 * @param float $score_loser
 *
 * @return array
 *   An array with two floats: the bonus and penaly scores.
 */
function game_score($score_winner, $score_loser) {
  $games_scores = [0, 0];

  // Simple score calculation: the winner takes 10% of the score of the loser.
  //  $score = round(0.1 * $score_loser, 2);
  //  $games_scores[0] = $score;
  //  $games_scores[1] = -1 * $score;

  // Calculate the bonus and penalty scores using the ELO system.
  // First calculation the expected outcome: Ea

  $expected_winner = 1 / (1 + pow(10, ($score_loser - $score_winner) / 400));
  $expected_loser = 1 / (1 + pow(10, ($score_winner - $score_loser) / 400));

  //  print $score_winner . PHP_EOL;
  //  print $score_loser . PHP_EOL;
  //
  //  print $expected_winner . PHP_EOL;
  //  print $expected_loser . PHP_EOL;

  $games_scores[0] = round(K_FACTOR * (1 - $expected_winner), 2);
  $games_scores[1] = round(K_FACTOR * (0 - $expected_loser), 2);

  return $games_scores;
}

function get_numb_games_per_player($games, $player1, $player2) {

  $matches = [];

  foreach ($games as $game) {
    if (($game[1] === $player1 || $game[1] === $player2) && ($game[2] === $player1 || $game[2] === $player2)) {
      $matches[] = $game;
    }
  }

  $won_p1 = 0;
  $won_p2 = 0;

  foreach ($matches as $match) {
    if ($match[1] === $player1) {
      $won_p1++;
    }

    if ($match[1] === $player2) {
      $won_p2++;
    }
  }

  $number_of_matches = count($matches);

  return [
    'p1' => $won_p1 === 0 ? 0 : round((100 / $number_of_matches) * $won_p1),
    'p1_matches' => $won_p1,
    'p2' => $won_p2 === 0 ? 0 : round((100 / $number_of_matches) * $won_p2),
    'p2_matches' => $won_p2,
    'matches' => $number_of_matches,
  ];
}
