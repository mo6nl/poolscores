<?php

/**
 * Games array: date, winning player, losing player.
 */
$games_json = file_get_contents('../db/games.json');
$games = json_decode($games_json, TRUE);


if (isset($_GET['winner']) && isset($_GET['loser'])) {

  $player1 = explode('|', $_GET['winner']);
  $player2 = explode('|', $_GET['loser']);

  $current_date = date('d-m-Y');
  $games[] = [ $current_date, $player1[0], $player2[0] ];

  file_put_contents('../db/games.json', json_encode($games));
}

// Return empty return value.
$return = $games;
print json_encode($return);

