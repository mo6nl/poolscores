<?php

include_once __DIR__ . '/func.php';
header("Content-type:application/json");
ini_set( 'serialize_precision', -1 );

$players_json = file_get_contents('../db/players.json');
$players = json_decode($players_json, TRUE);

$games_json = file_get_contents('../db/games.json');
$games = json_decode($games_json, TRUE);



if (isset($_GET['p1']) && isset($_GET['p2'])) {

  $player1 = explode('|', $_GET['p1']);
  $player2 = explode('|', $_GET['p2']);

  $original_p1 = (int) $player1[1];
  $original_p2 = (int) $player2[1];

  $p1 = game_score($original_p1, $original_p2);
  $p2 = game_score($original_p2, $original_p1);

  $game_percentage = get_numb_games_per_player($games, $player1[0], $player2[0]);

  $scores = [
    'p1' => [
      'win' => round($original_p1 + $p1[0]),
      'lose' => round($original_p1 - $p2[0]),
      'difference' => round($p1[0]),
      'original' => $original_p1,
      'percentage' => $game_percentage['p1'],
      'matches' => $game_percentage['p1_matches'],
    ],
    'p2' => [
      'win' => round($original_p2 + $p2[0]),
      'lose' => round($original_p2 - $p1[0]),
      'difference' => round($p2[0]),
      'original' => $original_p2,
      'percentage' => $game_percentage['p2'],
      'matches' => $game_percentage['p2_matches'],
    ],
    'matches' => $game_percentage['matches'],
  ];

  print json_encode($scores);

}
