<?php include_once __DIR__ . '/func.php';  ?>

<!doctype html>
<html>
<head>
<meta http-equiv="refresh" content="300">
<meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Jquery -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

  <!-- Datatables -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
  <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
  <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.min.css">
  <script src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap4.min.js"></script>

  <link rel="stylesheet" href="css/style.css">

  <!-- Vega charts -->
  <script src="https://cdn.jsdelivr.net/npm/vega@4.2.0"></script>

  <script>
    $(document).ready(function () {
      $(".enable-datatable").DataTable({
        responsive: true,
        pagingType: "simple"
      });
    });
  </script>
</head>
<body>

<?php

define('START_DATE', '01-01-2019');
define('PENALY_FACTOR', 0); // 0.3 percent per day.

/**
 * Players array: associative array: short name => long name.
 */
$players_json = file_get_contents('../db/players.json');
$players = json_decode($players_json, TRUE);

/**
 * Games array: date, winning player, losing player.
 */
$games_json = file_get_contents('../db/games.json');
$games = json_decode($games_json, TRUE);

$scores = [];
$won = [];
$lost = [];
foreach ($players as $player => $player_name) {
  $scores[$player] = 1200;
  $won[$player] = 0;
  $lost[$player] = 0;
}

$game_nr = 0;
$chart_data = [];

$max_score = 0;
$min_score = count($players) * 1200;

$last_day = '';
$times_played = [];
foreach ($games as $game) {
  if (($last_day != $game[0]) && ($last_day != '')) {
    // Each day a player loses points if he/she doesn't play.
    // Also: if a player plays more than 2 times he/she also loses points.
    $points_to_divide = 0;
    $penaly_players = 0;
    foreach ($players as $player => $player_name) {
      if (($times_played[$last_day][$player] < 1) || ($times_played[$last_day][$player] > 2)) {
        $points = PENALY_FACTOR * $scores[$player];
        $scores[$player] -= $points;
        $points_to_divide += $points;
        $penaly_players++;
      }
    }

    // Divide the points amongst all players.
    if ($penaly_players < count($players)) {
      $divide_per_player = $points_to_divide / (count($players) - $penaly_players);
      foreach ($players as $player => $player_name) {
        if (($times_played[$last_day][$player] >= 1) && ($times_played[$last_day][$player] <= 2)) {
          $scores[$player] += $divide_per_player;
        }
      }
    } else {
      $divide_per_player = $points_to_divide / count($players);
      foreach ($players as $player => $player_name) {
          $scores[$player] += $divide_per_player;
      }
    }

    $last_day = $game[0];
  }
  if ($last_day == '') {
    $last_day = $game[0];
  }

  // Skip empty games / weekends.
  if ($game[1] == '') {
    $chart_data[$game[0]] = $scores;
    $game_nr++;
    continue;
  }

  // Increment $times_played.
  foreach ($players as $player => $player_name) {
    if (!isset($times_played[$game[0]][$player])) {
      $times_played[$game[0]][$player] = 0;
    }
  }
  $times_played[$game[0]][$game[1]]++;
  $times_played[$game[0]][$game[2]]++;

  $game_scores = game_score($scores[$game[1]], $scores[$game[2]]);

  $scores[$game[1]] += $game_scores[0];
  $scores[$game[2]] += $game_scores[1];
  $games[$game_nr][3] = $game_scores[0];
  $won[$game[1]]++;
  $lost[$game[2]]++;
  $game_nr++;
  $chart_data[$game[0]] = $scores;

  $max = max($scores);
  $min = min($scores);

  if ($max > $max_score) {
    $max_score = $max;
  }

  if ($min < $min_score) {
    $min_score = $min;
  }
}

foreach ($players as $player => $player_name) {
  $scores[$player] = round($scores[$player]);
}

$data = [];
$name = 'table';
$values = [];

foreach ($players as $player => $player_name) {
  $value = new stdClass();
  $value->x = date('D d-m-Y', strtotime(START_DATE));
  $value->y = 1200;
  $value->c = $players[$player];
  $values[] = $value;
}

foreach ($chart_data as $date => $score_data) {
  foreach ($score_data as $player => $score) {
    $value = new stdClass();
    $value->x = date('D d-m-Y', strtotime($date));
    $value->y = $score;
    $value->c = $players[$player];
    $values[] = $value;
  }
}

$data_obj = new stdClass();
$data_obj->name = $name;
$data_obj->values = $values;
$data[] = $data_obj;
ksort($players);
?>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
    <div class="card">
      <div class="card-header">Progress</div>
      <div class="card-body">
        <div class="embed">
          <div id="line-chart" class="view"></div>
        </div>
      </div>
    </div>
    </div>
  </div>
<br>
  <div class="row">
  <div class="col-12">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="row">
            <div class="col-md-6">
              <div class="card">
                <div class="card-header">
                  Insert player 1
                </div>
                <div class="card-body">
                  <div class="form-group">
                    <select class="form-control player1">
                      <?php foreach ($players as $player => $player_name): ?>
                        <option value="<?php print $player;?>|<?php print $scores[$player]; ?>"><?php print $player_name; ?> (<?php print $scores[$player]; ?>)</option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <table class="table scores">
                    <tr>
                      <th>Winst</th>
                      <td width="100%" nowrap align="right">
                        <small class="badge badge-light"><span class="p1-orig"></span> + <span class="p1-difference"></span></small>
                      </td>
                      <td align="right"><span class="p1-win badge badge-success"></span></td>
                    </tr>
                    <tr>
                      <th>Verlies</th>
                      <td nowrap align="right">
                        <small class="badge badge-light"><span class="p1-orig"></span> - <span class="p2-difference"></span></small>
                      </td>
                      <td align="right"><span class="p1-lose badge badge-danger"></span></td>
                    </tr>
                    <tr>
                      <th>Factor</th>
                      <td></td>
                      <td align="right"><span class="p1-difference badge badge-primary"></span></td>
                    </tr>
                    <tr>
                      <th nowrap="nowrap">Won <span class="p1-matches"></span></th>
                      <td colspan="2" align="right"><span class="p1-percentage badge badge-dark"></span></td>
                    </tr>
		    <tr>
                      <th nowrap="nowrap">Won this game</th>
                      <td colspan="2" align="right"><button class="btn btn-secondary player-1-won">Add</button></td>
		    </tr>
                  </table>
                </div>
              </div>
            </div>
            <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                Insert player 2
              </div>
              <div class="card-body">
                <div class="form-group">
                  <select class="form-control player2">
                    <?php foreach ($players as $player => $player_name): ?>
                      <option
                          value="<?php print $player;?>|<?php print $scores[$player]; ?>"><?php print $player_name; ?>
                        (<?php print $scores[$player]; ?>)
                      </option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <table class="table scores">
                  <tr>
                    <th>Verlies</th>
                    <td nowrap align="right">
                      <small class="badge badge-light"><span class="p2-orig"></span> - <span class="p1-difference"></span></small>
                    </td>
                    <td align="right"><span class="p2-lose badge badge-danger"></span></td>
                  </tr>
                  <tr>
                    <th>Winst</th>
                    <td width="100%" nowrap align="right">
                      <small class="badge badge-light"><span class="p2-orig"></span> + <span class="p2-difference"></span></small>
                    </td>
                    <td align="right"><span class="p2-win badge badge-success"></span></td>
                  </tr>
                  <tr>
                    <th>Factor</th>
                    <td align="right"></td>
                    <td align="right"><span class="p2-difference badge badge-primary"></span></td>
                  </tr>
                  <tr>
                    <th nowrap="nowrap">Won <span class="p2-matches"></span></th>
                    <td colspan="2" align="right"><span class="p2-percentage badge badge-dark"></span></td>
                  </tr>
		    <tr>
                      <th nowrap="nowrap">Won this game</th>
                      <td colspan="2" align="right"><button class="btn btn-secondary player-2-won">Add</button></td>
		    </tr>
                </table>
              </div>
            </div>
          </div>
          </div>
          <br>
          <div class="row">
            <div class="col-12" style="text-align: center">
              <button class="play-ball btn btn-primary">Uitkomst <span class="matches"></span></button>
            </div>
          </div>
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>

  </div>
  <br>

</div>
<script>
  var spec = {
        "$schema": "https://vega.github.io/schema/vega/v4.json",
        "autosize": { type: "fit", resize: false },

        "data": <?php print json_encode($data) . ',' . PHP_EOL; ?>

        "scales": [
          {
            "name": "x",
            "type": "point",
            "range": "width",
            "domain": {"data": "table", "field": "x"}
          },
          {
            "name": "y",
            "type": "linear",
            "range": "height",
            "nice": true,
            "zero": false,
            "domain": [<?php print floor($min_score); ?>, <?php print ceil($max_score); ?>]
          },
          {
            "name": "player",
            "type": "ordinal",
            "range": { "scheme": "category10" },
            "domain": {"data": "table", "field": "c"}
          }
        ],

        "axes": [
          {
            "orient": "bottom",
            "scale": "x",
            "grid": true,
            "gridColor": '#f5f5f5',
            "labelOverlap": "greedy"
          },
          {
            "orient": "left",
            "grid": true,
            "gridColor": '#f5f5f5',
            "scale": "y"
          }
        ],

      "legends": [
        {
          "stroke": "player",
          "title": "Players",
          "direction": "horizontal",
          "orient": "bottom",
          "padding": 4,
          "encode": {
            "symbols": {
              "enter": {
                "strokeWidth": {"value": 7},
                "size": {"value": 50}
              }
            }
          }
        }
      ],

        "marks": [
          {
            "type": "group",
            "from": {
              "facet": {
                "name": "series",
                "data": "table",
                "groupby": "c"
              }
            },
            "marks": [
              {
                "type": "line",
                "from": {"data": "series"},
                "encode": {
                  "enter": {
                    "interpolate": {"value": "monotone"},
                    "x": {"scale": "x", "field": "x"},
                    "y": {"scale": "y", "field": "y"},
                    "stroke": {"scale": "player", "field": "c"},
                    "strokeWidth": {
                      "value": 2
                    }
                  },
                  // "update": {
                  //   "fillOpacity": {"value": 1}
                  // },
                  // "hover": {
                  //   "fillOpacity": {"value": 0.5}
                  // }
                }
              }
            ]
          }
        ]
      }
  ;

  var view = new vega.View(vega.parse(spec), {
    loader: vega.loader({baseURL: '/vega/'}),
    logLevel: vega.Warn,
    renderer: 'canvas'
  }).initialize('#line-chart').hover().run();

  var resizeTimer;

  $(window).on("resize", function(e) {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function() {

      const $element = $(".embed");
      const width = $element.width();
      const height = $element.height();
      view
          .width(width)
          .height(height)
          .run();
    }, 500);
  });

  $(window).on("load", function(e) {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function() {

      const $element = $(".embed");
      const width = $element.width();
      const height = $element.height();
      view
          .width(width)
          .height(height)
          .run();
    }, 100);

    $('.play-ball').on('click', function() {
      const val_p1 = $('.player1').val();
      const val_p2 = $('.player2').val();
      $.getJSON("ajax.php?p1=" + val_p1 + "&p2=" +  val_p2, function(data) {
        $('.p1-win').html(data.p1.win);
        $('.p1-lose').html(data.p1.lose);
        $('.p2-win').html(data.p2.win);
        $('.p2-lose').html(data.p2.lose);
        $('.p1-orig').html(data.p1.original);
        $('.p2-orig').html(data.p2.original);

        $('.p1-percentage').html(data.p1.percentage + '%').css('width', data.p1.percentage + '%');
        $('.p2-percentage').html(data.p2.percentage + '%').css('width', data.p2.percentage + '%');

        $('.p1-difference').html(data.p1.difference);
        $('.p2-difference').html(data.p2.difference);

        $('.p1-matches').html('(' + data.p1.matches + ')');
        $('.p2-matches').html('(' + data.p2.matches + ')');
        $('.matches').html('(' + data.matches + ')');

        $('.scores').css('display', 'table');
      });
    });

    $('.player-1-won').on('click', function() {
      const val_p1 = $('.player1').val();
      const val_p2 = $('.player2').val();
      $.getJSON("game.php?winner=" + val_p1 + "&loser=" +  val_p2, function(data) {
        location.reload();
      });
    });

    $('.player-2-won').on('click', function() {
      const val_p1 = $('.player1').val();
      const val_p2 = $('.player2').val();
      $.getJSON("game.php?winner=" + val_p2 + "&loser=" +  val_p1, function(data) {
        location.reload();
      });
    });
  });

</script>
</div>


<?php arsort($scores); ?>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">Players</div>
        <div class="card-body">
          <table data-order="[[ 1, &quot;desc&quot; ]]" class="table enable-datatable">
            <thead>
              <tr>
                <th>Naam</th><th>Score</th><th>W/L Ratio</th><th>Games</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($scores as $name => $score): ?>
                <tr>
                  <td><?php print $players[$name]; ?></td>
                  <td align="right"><samp><?php print $score; ?></samp></td>
                  <?php if ($won[$name]+$lost[$name] > 0): ?>
                    <td align="right"><samp><?php print number_format(round($won[$name] / (max(1,$lost[$name])),2),2); ?></samp></td>
                  <?php else: ?>
                    <td align="right"><samp>&nbsp;&mdash;</samp></td>
                  <?php endif; ?>
                  <?php $total = $won[$name] + $lost[$name]; ?>
                  <td align="right"><samp><?php print $total; ?></samp></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="card">
        <div class="card-header">Games</div>
        <div class="card-body">
          <table data-order="[[ 0, &quot;desc&quot; ]]" data-page-length="10" class="table enable-datatable">
            <thead>
              <tr>
                <th>Datum</th><th>Winnaar</th><th>Verliezer</th><th>Score</th>
              </tr>
            </thead>
            <tbody>
              <?php for ($i = count($games); $i > 0; $i--): ?>
                <?php $game = $games[$i-1]; ?>
                <tr class="game-<?php echo $game[1] . '-' . $game[2]; ?>">
                  <td data-order="<?php print strtotime($game[0]); ?>"><?php print $game[0]; ?></td>
                  <td><?php print $players[$game[1]]; ?></td>
                  <td><?php print $players[$game[2]]; ?></td>
                  <td align="right"><samp><?php print round($game[3]); ?></samp></td>
                </tr>
              <?php endfor; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
